package com.hendisantika.springbootfileupload;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@SpringBootApplication
@Slf4j
@EnableDiscoveryClient
public class SpringBootFileUploadApplication {

    public static void main(String[] args) {
        log.info("SpringBootFileUpload Application is Starting...");
        try {
            SpringApplication.run(SpringBootFileUploadApplication.class, args);
        } catch (Exception e) {
            log.error("Error occurred while starting SpringBootFileUpload");
        }
        log.info("SpringBootFileUpload Application Started..");
    }

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(-1);
        return multipartResolver;
    }

}
